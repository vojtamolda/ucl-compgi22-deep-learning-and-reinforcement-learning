# University College London Course COMPGI22 - Advanced Deep Learning and Reinforcement Learning (2017/18)


Syllabus, reading materials and additional information can be downloaded from the [course webpage](http://www.cs.ucl.ac.uk/syllabus/compgi/compgi22_advanced_deep_learning_and_reinforcement_learning/).
 - Lecture videos: [YouTube](https://www.youtube.com/playlist?list=PLqYmG7hTraZDNJre23vqCGIVpfZ_K2RZs)
 - Reading List: [UCL Library](http://readinglists.ucl.ac.uk/modules/compgi22.html)


## Course Work

Course work will focus on the practical implementation of deep neural network training and reinforcement learning algorithms and architectures in TensorFlow. There will be 4 homeworks for each course path.

 - [ ] Deep Learning 1 - [`dl/cw1.ipynb`](dl/cw1.ipynb)
 - [ ] Deep Learning 2 - [`dl/cw2.ipynb`](dl/cw2.ipynb)
 - [ ] Deep Learning 3 - [`dl/cw3.ipynb`](dl/cw3.ipynb)
 - [ ] Deep Learning 4 - [`dl/cw3.ipynb`](dl/cw4.ipynb)
 - [ ] Reinforcement Learning 1 - [`rl/cw1.ipynb`](rl/cw1.ipynb)
 - [ ] Reinforcement Learning 2 - [`rl/cw2.ipynb`](rl/cw2.ipynb)
 - [ ] Reinforcement Learning 3 - [`rl/cw3.ipynb`](rl/cw3.ipynb)
 - [ ] Reinforcement Learning 4 - [`rl/cw4.ipynb`](rl/cw4.ipynb)


## Prerequisites

The prerequisites are probability, calculus, linear algebra and [COMPGI01 Supervised Learning](http://www.cs.ucl.ac.uk/syllabus/compgi/compgi01_supervised_learning/) or [COMPGI08 Graphical Models](http://www.cs.ucl.ac.uk/syllabus/compgi/compgi08_graphical_models/) or [COMPGI18 Probabilistic and Unsupervised Learning](http://www.cs.ucl.ac.uk/syllabus/compgi/compgi18_probabilistic_and_unsupervised_learning/).


## Content

The course has two interleaved parts that converge towards the end of the course. One part is on machine learning with deep neural networks, the other part is about prediction and control using reinforcement learning. The two strands come together when we discuss deep reinforcement learning, where deep neural networks are trained as function approximators in a reinforcement learning setting.

The deep learning stream of the course will cover a short introduction to neural networks and supervised learning with TensorFlow, followed by lectures on convolutional neural networks, recurrent neural networks, end-to-end and energy-based learning, optimization methods, unsupervised learning as well as attention and memory. Possible applications areas to be discussed include object recognition and natural language processing.

The reinforcement learning stream will cover Markov decision processes, planning by dynamic programming, model-free prediction and control, value function approximation, policy gradient methods, integration of learning and planning, and the exploration/exploitation dilemma. Possible applications to be discussed include learning to play classic board games as well as video games.


